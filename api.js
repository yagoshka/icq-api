(function(mailru, $){
    $ = $ || {};
    /**
     * @global
     * @namespace
     * @alias mailru
     */
    var api = {},
        utils = {},
        callbacks = {},
        empty = function(){return false;},
        backButtonHandler = function(){return false;},
        defaultBackButtonHandler = function(){return false;},
        onPushCallback = function(){return false;},
        onPauseCallback = function(){return false;},
        onResumeCallback = function(){return false;},
        customBackButtonHandler = function(){return false;};

    function isUrl(url) {
        var parser = utils.parseURL(url);
        return parser.protocol;
    }

    function getValueByKey(o,k) {
        for (var tmp = k.split('.'),l = tmp.length, i = 0;i<l;i++) {
            if (!(o = o[tmp[i]])) break;
        }
        return o;
    }

    var tmp, tmpl, getData = {}, dataParseFuncs = {
        ref: function(v){
            v = v.split(',');
            v[0] = v[0].replace(/^(?:sidebar|top|chat)/,function(t){v.type=t; return ''; });
            return v;
        },
        data: function(v){
            var d = {},hasKey;
            v.replace(/([^=]+)=([^=]+)/g,function(i,k,vl){hasKey = true;d[k] = decodeURIComponent(vl)});
            return hasKey? d : v;
        }
    };
    tmp = location.search.match(/[^=&?]+(?:=[^&]+)?/g)||[];
    tmpl = tmp.length;
    while (tmpl--) {
        tmp[tmpl].replace(/([^=]+)(?:=([^=]+))?/g,function(i,k,v){
            k = decodeURIComponent(k);
            getData[k] = typeof (v) === 'undefined'? true : dataParseFuncs[k]? dataParseFuncs[k](decodeURIComponent(v)) : decodeURIComponent(v);
        });
    }

    var clientVer = 1*(getData.clientVer || '0').replace('.',',').replace(/[^0-9,]/g,'').replace(',','.'),
        testedParams = [
            'message.send-base64-5.12'
        ];
    supportedParams = {};
    tmpl = testedParams.length;
    while (tmpl--) {
        tmp = testedParams[tmpl].split('-');
        tmp = [tmp[0], {name: tmp[1], ver: 1 * tmp[2]}];
        (supportedParams[tmp[0]] || (supportedParams[tmp[0]] = [])).push(tmp[1]);
    }

    var validFuncs = {
        "string": function (param) {
            return typeof param === 'string';
        },
        "number": function(param) {
            return (typeof +param === 'number' && isNaN(+param) === false);
        },
        "url": function(param) {
            return isUrl(param);
        },
        "function": function(param) {
            return typeof param === 'function';
        }
    };

    function isValidParams(rules,params,errorCb,lenObj) {
        function isPresent(param) {
            return param !== undefined && param !== null && param !== '' && !(typeof param == 'number' && isNaN(param));
        }

        if (arguments.length<3 && typeof params == 'function') {
            lenObj = errorCb;
            errorCb = params;
            params = undefined;
        }

        var rule, name, names, tmp, ruleList, param, i, l, n, isArray,
            maxLengthParam = '', maxLen = 0, valid = false, currentLen = 0, fullLen = 0;
        for (rule in rules) {
            if (!rules.hasOwnProperty(rule)) continue;
            names = rules[rule];
            ruleList = rule.split(',');
            if (!(names instanceof Array)) names = [names];
            for (n = 0; n < names.length; n++) {
                name = names[n];
                if (params) {
                    if (name.indexOf('.')>-1) {
                        param = getValueByKey(params,name);
                    } else {
                        param = params[name];
                    }
                } else {
                    param = name;
                    name = 0;
                }
                for (i = 0; i < ruleList.length; i++) {
                    rule = ruleList[i];

                    if (rule !== 'required' && !isPresent(param)) {
                        continue;
                    }

                    isArray = rule.indexOf('[]')>-1 && ((rule = rule.replace('[]','')),true);

                    switch(rule) {
                        case "required":
                            if (name.indexOf && ~name.indexOf('|')) {
                                param = name.split('|');
                                l = param.length;
                                while (l--) {
                                    if (valid = isPresent(params[param[l]])) {
                                        break;
                                    }
                                }
                            } else {
                                valid = isPresent(param);
                            }
                            break;
                        default:
                            if (isArray) {
                                if (valid = (param instanceof Array)) {
                                    for (var _i= 0,_l=param.length;_i<_l;_i++) {
                                        if (!(valid = validFuncs[rule](param[_i]))) break;
                                    }
                                }
                            } else {
                                valid = validFuncs[rule](param);
                                rule !== 'function' && name !== 'base64' && (fullLen += (currentLen = encodeURIComponent(param).replace(/%../g,'x').length));
                            }
                    }
                    if (!valid) {
                        errorCb(rule,name,param);
                        return false;
                    }
                }
                if (maxLen < currentLen) {
                    maxLen = currentLen;
                    maxLengthParam = name;
                }
            }
            if (fullLen > 4e3) {
                errorCb('Any parameter is too long! May be it\'s "' + maxLengthParam + '"');
                return false;
            }
            if (lenObj) {
                lenObj.size = fullLen;
                lenObj.name = maxLengthParam;
                lenObj.maxSize = maxLen;
            }
        }

        return valid;
    }

    function prepareParams(params,cb,map,def,errorcb,sizeData,method) {
        if (typeof def == 'function') {
            sizeData = errorcb;
            errorcb = def;
            def = undefined;
        }
        if (typeof params == 'function') {
            params = {callback: params};
        }
        params = params || {};
        params.callback = params.callback || cb;

        if (def) for (var i in def) {
            params[i] = params[i] || def[i];
        }

        var s, sn, m, l;
        if (s = supportedParams[method]) {
            l = s.length;
            while (l--) {
                sn = s[l];
                if ((!sn.name || (sn.name in params)) && sn.ver>clientVer) {
                    m = (sn.name? 'Parameter "'+sn.name+'" in m' : 'M')+
                    'ethod "mailru.'+method+'" is supported only from client version '+
                    sn.ver+', but your client version '+clientVer+'!';
                    (errorcb || error)(m);
                    if (typeof params.callback === 'function') {
                        setTimeout(function(){
                            params.callback({
                                status: 'error',
                                message: m
                            });
                        },0);
                    }
                    return false;
                }
            }
        }

        if (!isValidParams(map,params,errorcb || error,sizeData)) {
            return false;
        }
        return params;
    }

    function prepareFunc(map,method,def,prepare,realmethod) {
        if (typeof def == 'function') {
            prepare = def;
            def = undefined;
        }
        if (typeof method == 'function') {
            prepare = method;
            method = undefined;
        }
        return function(params,cb){
            var sizeData = {size:0,maxSize:0};
            if (params = prepareParams(params,cb,map,def,false,sizeData,realmethod||method)) {
                prepare && prepare(params,sizeData);
                if (sizeData.size > 4e3) {
                    return error('Any parameter is too long! May be it\'s "' + sizeData.name + '"');
                }
                method && api.invoke(method, {
                    params: params,
                    cb: params.callback
                });
            } else {
                return false;
            }
        }
    }

    function addCallback(callback) {
        var cbid = utils.randomId();

        api.callbacks[cbid] = function(data) {
            delete api.callbacks[cbid];

            try {
                data = JSON.parse(data.replace(/\n/g,'\\n'));
            } catch(e) {

            }

            if (callback) {
                callback.call(window, data);
            }
        };

        return 'mailru.callbacks["' + cbid + '"]';
    }

    function error(rule,name,param) {
        var err;
        if (api.isDev) {
            if (arguments.length == 1) {
                err = typeof rule == 'string'? new Error(rule) : rule;
            } else {
                if (rule == 'required') {
                    err = new Error(isNaN(name)? 'Param '+name.replace(/\|/g,' or ')+' is required' : 'Missing required argument');
                } else {
                    err = new TypeError((isNaN(name)? 'Param '+name : 'Argument')+' must be a '+rule+', but is '+typeof(param));
                }
            }

            throw err;
        } else {
            return false;
        }
    }

    function saveCall(f,args,th) {
        try {
            return f.apply(th||this,args||[]);
        } catch (err) {
            error(err);
        }
    }

    utils.randomize = function() {
        return Math.floor(Math.random() * 10000);
    };
    utils.randomId = function() {
        return 'ID'+Math.random().toString(32).substring(2);
    };
    utils.parseURL = function(url) {
        var parser = document.createElement('a');
        parser.href = url;
        return parser;
    };
    utils.makeGet = function(obj) {
        if ('param' in $) {
            return $.param(obj);
        }

        var r = [];
        for (var k in obj) {
            if (!obj.hasOwnProperty(k)) continue;
            r[r.length] = k + '=' + encodeURIComponent(obj[k]);
        }
        return r.join('&');
    };

    if (mailru && mailru.invokeMessengerAPI) {
        window.oldMailru = mailru;

        api.invoke = function (methodName, data) {
            if (data && typeof data.cb == 'function')
                mailru.invokeMessengerAPI(methodName, JSON.stringify(data.params), addCallback(data.cb));
            else
                mailru.invokeMessengerAPI(methodName, JSON.stringify(data && data.params), 'mailru.emptycallback');
        };

        backButtonHandler = api.backButtonHandler = function() {
            var preventDefaultHandle = saveCall(customBackButtonHandler);
            if (!preventDefaultHandle) {
                mailru.backButtonHandler();
            }
            return preventDefaultHandle;
        };

        defaultBackButtonHandler = function() {
            mailru.backButtonHandler();
        }
    } else {
        api.invoke = function (methodName, data) {
            data = data || {};

            if (window.console) {
                window.console.log(methodName, data.params,addCallback(data.cb));
            }
            data.cb && data.cb({
                callbackTest: 'passed'
            });
        };
    }

    api.onPushData = function(d) {
        saveCall(onPushCallback,[d]);
    };

    api.onPause = api.onStop = function() {
        saveCall(onPauseCallback);
    };

    api.onResume = api.onStart = function() {
        saveCall(onResumeCallback);
    };

    api.getData = getData;

    api.utils = utils;
    api.callbacks = {};
    api.emptycallback = empty;

    /**
     * @namespace
     */
    api.app = {};
    /**
     * @namespace
     */
    api.device = {};
    /**
     * @namespace
     */
    api.message = {};
    /**
     * @namespace
     */
    api.users = {};
    /**
     * @namespace
     */
    api.friends = {};
    /**
     * @namespace
     */
    api.photos = {};

    api.camera = {};

    /**
     * Закрывает приложение и возвращает пользователя в предыдущий контекст (каталог приложений/сообщение)
     * @function close
     * @memberof mailru.app
     * @param {boolean} [force] - если передано значение true происходит форсированное закрытие приложение,
     * можно использовать в случае, если обычное закрытие по какой-то причине не работает.
     */
    api.app.close = function(force) {
        if (force) {
            api.invoke('app.close');
        } else {
            defaultBackButtonHandler();
        }
    };
    /**
     * Объект для работы с экранной клавиатурой
     * @namespace keyboard
     * @name keyboard
     * @memberof mailru.app
     */
    api.app.keyboard = {};
    /**
     * Скрывает экранную клавиатуру
     * @function hide
     * @memberof mailru.app.keyboard
     */
    api.app.keyboard.hide = function() {
        api.invoke('keyboard.hide');
    };
    /**
     * Показывает экранную клавиатуру
     * @function show
     * @memberof mailru.app.keyboard
     */
    api.app.keyboard.show = function() {
        api.invoke('keyboard.show');
    };

    /**
     * Устанавливает или сбрасывает обработчик нажатия кнопки Back
     * @function back
     * @name back(1)
     * @memberof mailru.app
     * @param {function|boolean} handler -  функция, которая будет вызвана при нажатии кнопки Back
     * если handler возвращает true - стандартное поведение при клике на Back будет игнорироваться.
     * Если в качестве параметра handler передано значение boolean равное false, то текущий, установленный обработчик
     * кнопки Back будет удален.
     * @return {boolean} Если false значит метод выполнен с ошибками.
     * @example
     * var isFirstPage = true;
     * mailru.app.back(function(){
     *   if (!isFirstPage) {
     *     alert('Go to previous page');
     *     return true;
     *   } else {
     *     alert('Go away from application');
     *     return false;
     *   }
     * });
     */
    /**
     * При вызове без параметров имитирует нажатие на кнопку Back
     * @function
     * @name back(2)
     * @memberof mailru.app
     * @return {boolean} =false Возвращает значение, которое вернул обработчик нажатия кнопки Back
     */
    api.app.back = api.customBackButtonHandler = function(handler) {
        if (handler === false) handler = function(){return false};
        if (handler) {
            if (isValidParams({
                    'required,function': handler
                }, function(){
                    error.apply(null,arguments);
                })) {
                customBackButtonHandler = handler;
                return true;
            } else {
                return false;
            }
        } else {
            return backButtonHandler();
        }
    };
    /**
     * Устанавливает обработчик данных уведомления, который вызывается при получении нового уведомления
     * при активном экране с приложением
     * @function onPush
     * @memberof mailru.app
     * @param {function} cb - обработчик данных, пришедших в пуше
     * @param {object} cb.data - данные, которые были переданы в параметре data при отправке уведомления
     * @return {boolean} Если false значит метод выполнен с ошибками.
     */
    api.app.onPush = api.app.setOnPushDataCallback = function(cb) {
        if (cb === false) cb = function(){};
        if (isValidParams({
                'required,function': cb
            }, function(){
                error.apply(null,arguments);
            })) {
            onPushCallback = cb;
            return true;
        } else {
            return false;
        }
    };
    /**
     * Устанавливает обработчик состояния активности приложения.
     * Приложение является активным, когда показывается пользователю.
     * Если открывается диалог через api, пользователь сворачивает клиент, etc - приложение уходит в бэкграунд и вызывается onPause.
     * После возвращения пользователя в приложение - вызывается [onResume]{@link mailru.app.onResume}
     * @function onPause
     * @memberof mailru.app
     * @param {function} cb - обработчик состояния активности приложения.
     * @return {boolean} Если false значит метод выполнен с ошибками.
     */
    api.app.onPause = function(cb) {
        if (cb === false) cb = function(){};
        if (isValidParams({
                'required,function': cb
            }, function(){
                error.apply(null,arguments);
            })) {
            onPauseCallback = cb;
            return true;
        } else {
            return false;
        }
    };
    /**
     * Устанавливает обработчик состояния активности приложения.
     * Приложение является активным, когда показывается пользователю.
     * Если открывается диалог через api, пользователь сворачивает клиент, etc - приложение уходит в бэкграунд и вызывается [onPause]{@link mailru.app.onPause}.
     * После возвращения пользователя в приложение - вызывается onResume
     * @function onResume
     * @memberof mailru.app
     * @param {function} cb - обработчик состояния активности приложения.
     * @return {boolean} Если false значит метод выполнен с ошибками.
     */
    api.app.onResume = function(cb) {
        if (cb === false) cb = function(){};
        if (isValidParams({
                'required,function': cb
            }, function(){
                error.apply(null,arguments);
            })) {
            onResumeCallback = cb;
            return true;
        } else {
            return false;
        }
    };
    api.app.loaded = function() {
        try {
            api.invoke('app.loaded');
        } catch (e) {}
    };

    /**
     * Открывает внешний ресурс
     * @function openUrl
     * @memberof mailru.app
     * @param {{url: url}} params
     * @param {url} params.url - URL адрес внешнего ресурса.
     * @param {function} params.callback
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.app.openUrl = prepareFunc({
        'required,url': 'url'
    }, 'app.openUrl');

    /**
     * @callback mailru.device~callback
     * @param {{}} result - Состояние ориентации устройства
     * @param {string} result.orientation - текущая ориентация, может иметь значения landscape, portrait, или unspecified.
     */
    /**
     * Возвращает ориентацию и блокировку смены ориентации
     * @function orientation
     * @name orientation(1)
     * @memberof mailru.device
     * @param {mailru.device~callback} callback - принемает состояние ориентации устройства.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    /**
     * Устанавливает ориентацию и блокировку смены ориентации
     * @function
     * @name orientation(2)
     * @variation 2
     * @memberof mailru.device
     * @param {{}} params
     * @param {string} [params.orientation] - новая ориентация, может иметь значения landscape, portrait, или unspecified.
     * @param {mailru.device~callback} [params.callback] - принемает состояние ориентации устройства.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.device.orientation = prepareFunc({
        'string': 'orientation',
        'function': 'callback'
    },'device.orientation');

    /**
     * Выполняет запрос на отправку сообщения пользователю из контакт-листа юзера.
     * Обязательным параметром является один из params.text, params.url, params.title, params.image, params.base64.
     * @function send
     * @memberof mailru.message
     * @param {{}} params
     * @param {string} params.text - текст сообщения
     * @param {url} params.url|image - ссылка на картинку
     * @param {string_base64} params.base64 - изображение в формате base64
     * @param {string} params.title - заголовок сообщения
     * @param {string} [params.uin] - uin получателя, если передан - будет предвыбран на экране выбора получателей
     * @param {object} [params.data] - данные, которые приложение получит при открытии
     * @param {{text: string}} [params.fallback]
     * @param {string} [params.fallback.text] - текст, который увидит пользователь клиента, не поддерживающего сообщения из приложений (старые клиенты)
     * @param {bool} [params.only_compatible] - фильтрация списка получателей, если не передан uin. only_compatible=0 - показываются все получатели, only_compatible=1 - показываются только получатели, которые поддерживают приложения хотя бы на одном инстансе. необязательный параметр
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.message.send({
     *   uin: 111111111,
     *   title: "My Message",
     *   text: "Hello! it's my photo",
     *   image: "http://mydomen.ru/photos/my_photo.png",
     *   fallback: {
     *     text: "Чтобы увидеть фото, пройдите по ссылке"
     *   },
     *   data: { img: "http://mydomen.ru/photos/my_photo.png" }
     * });
     */
    api.message.send = prepareFunc({
        'required': 'text|url|title|image|base64',
        'string': ['text','title','uin','base64','fallback.text'],
        'url': ['url','image'],
        'function': 'callback'
    },'message.send',function(params,sizeData){
        if (typeof params.data === 'object') {
            params.data = utils.makeGet(params.data);
        }
        if (params.data) {
            var size = encodeURIComponent(params.data).replace(/%../g,'x').length;
            sizeData.size += size;
            if (sizeData.maxSize < size) {
                sizeData.name = 'data';
                sizeData.maxSize = size;
            }
        }
    });
    /**
     * Открытие диалога с пользователем
     * @function openChat
     * @memberof mailru.message
     * @param {{uin: string}} params
     * @param {string} params.uin
     * @param {function} params.callback
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.message.openChat = prepareFunc({
        'string': 'uin',
        'function': 'callback'
    },'message.openChat');

    /**
     * @callback mailru.users~getInfo_callback
     * @param {object} result - Информация о пользователе
     * @param {string} result.first_name - имя
     * @param {string} result.last_name - фамилия
     * @param {string} result.nick - ник
     * @param {number} result.sex - пол --- 0 - мужской, 1 - женский, 2 - не задан
     * @param {date} result.birthday - дата рождения в формате dd.mm.yyyy
     * @param {object} result.avatars
     * @param {url} result.avatars.big - ссылка на аватарку
     * @param {url} result.avatars.small - ссылка на аватарку
     * @param {number} result.online - статус пользователя - 0 - оффлайн, 1 - в сети
     * @param {string} result.uin - uin
     * @param {number} result.contacts_count - кол-во контактов в кл,
     * @param {number} result.apps_support - флаг поддержки клиентом приложений
     */
    /**
     * Возвращает инфо по текущему пользователю
     * @function getInfo
     * @name getInfo(1)
     * @memberof mailru.users
     * @param {mailru.users~getInfo_callback} callback - принемает информацию о пользователе.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    /**
     * Возвращает инфо по указанному uin.
     * @function
     * @name getInfo(2)
     * @variation 2
     * @memberof mailru.users
     * @param {{}} params
     * @param {string} [params.uin] - uin пользователя, по которому нужно получить информацию.
     * @param {mailru.users~getInfo_callback} params.callback - принемает информацию о пользователе.
     * @example
     * mailru.users.getInfo(function(info){alert('Hello '+info.first_name)});
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.users.getInfo = prepareFunc({
        'required,function': 'callback',
        'string': 'uin'
    },'users.getInfo');

    /**
     * @callback mailru.users~getLocation_callback
     * @param {{}} result - Текущие гео-данные пользователя
     * @param {{lat: float, lon: float}} result.location - текущие координаты формата {lat: float, lon: float}
     * @param {string} result.status - статус выполнения [success/fail]
     */
    /**
     * Возвращает текущие гео-данные пользователя.
     * @function getLocation
     * @memberof mailru.users
     * @param {mailru.users~getLocation_callback} callback - принемает информацию о пользователе.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.users.getLocation(function(data){
     *   if (data.status == 'success') {
     *     alert('Your location is lat: '+data.location.lat+', lon: '+data.location.lon)});
     *   } else {
     *     alert('Sorry, '+data.message);
     *   }
     * });
     */
    api.users.getLocation = prepareFunc({
        'required,function': 'callback'
    },'users.getLocation');

    /**
     * @callback mailru.friends~add_callback
     * @param {{}} result
     * @param {string} result.status - статус выполнения [success/already/error/fail],
     * success - успешное добавление, already - uin уже есть в контакт-листе,
     * fail - пользователь отказался добавлять переданый uin,
     * error - ошибка в процессе выполнения вызова
     */
    /**
     * Запрос на добавление в контакт-лист.
     * показывает диалог добавления пользователя в контакт лист.
     * @function add
     * @memberof mailru.friends
     * @param {{}} params
     * @param {string} params.uin - uin пользователя, которого нужно добавить в друзья активному пользователю.
     * @param {mailru.friends~add_callback} [params.callback] - функция для обработки результата.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.friends.add({
	 *   uin: 111111111,
	 *   callback: function(res){
	 *     var messages = {
     *       success: 'User is successful added',
     *       already: 'User already in your contactlist',
     *       fail: 'User reject your request',
     *       error: 'Sorry, we have any error'
     *     };
     *     alert(messages[res.status] || messages.error);
     *   }
     *});
     */
    api.friends.add = prepareFunc({
        'required,string': 'uin',
        'function': 'callback'
    },'friends.add');

    /**
     * @callback mailru.friends~pick_callback
     * @param {object} result
     * @param {(Object[])} result.users - информация о выбранных пользователях
     * @param {string} result.users.first_name - имя
     * @param {string} result.users.last_name - фамилия
     * @param {string} result.users.nick - ник
     * @param {number} result.users.sex - пол --- 0 - мужской, 1 - женский, 2 - не задан
     * @param {date} result.users.birthday - дата рождения в формате dd.mm.yyyy
     * @param {object} result.users.avatars
     * @param {url} result.users.avatars.big - ссылка на аватарку
     * @param {url} result.users.avatars.small - ссылка на аватарку
     * @param {number} result.users.online - статус пользователя - 0 - оффлайн, 1 - в сети
     * @param {string} result.users.uin - uin
     * @param {number} result.users.contacts_count - кол-во контактов в кл,
     * @param {number} result.users.apps_support - флаг поддержки клиентом приложений
     * @param {string} result.status - статус выполнения [success/error/fail]
     */
    /**
     * Показывает диалог выбора пользователей из контакт-листа. после выбора и закрытия диалога приложение получает список выбранных пользователей
     * @function pick
     * @name pick(1)
     * @memberof mailru.friends
     * @param {mailru.friends~pick_callback} callback - принемает информацию о пользователях.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    /**
     * Позволяет задать параметры фильтрации для выбора пользователей из контакт-листа.
     * @function
     * @name pick(2)
     * @memberof mailru.friends
     * @variation 2
     * @param {{}} params
     * @param {number} [params.limit] - ограничение кол-ва выбираемых людей. необязательный параметр. по умолчанию - нет ограничения
     * @param {boolean} [params.only_compatible] - фильтрация списка получателей, если не передан uin. only_compatible=0 - показываются все получатели, only_compatible=1 - показываются только получатели, которые поддерживают приложения хотя бы на одном инстансе. необязательный параметр
     * @param {string[]} [params.preselect] - массив uin'ов, которые будут заранее помечены выбраннными.
     * @param {mailru.friends~pick_callback} params.callback - принемает информацию о выбранных пользователях.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.friends.pick = prepareFunc({
        'number': 'limit',
        'string[]': 'preselect',
        'required,function': 'callback'
    },'friends.picker',{limit:0},undefined,'friends.pick');

    /**
     * Запрос на получение данных о пользователях из контакт-листа юзера.
     * Метод возвращает uin'ы и минимальную инфу о пользователях из кл текущего пользователя.
     * @function getInfo
     * @name getInfo(1)
     * @memberof mailru.friends
     * @param {mailru.friends~pick_callback} callback - принемает информацию о пользователях.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.friends.getInfo(function(data){
     *   if (data.status == 'success') {
     *     alert('You have '+data.users.length+' friends');
     *   } else {
     *     alert('Sorry, we have any error');
     *   }
     * });
     */
    /**
     * Метод возвращает uin'ы и минимальную инфу о пользователях из кл текущего пользователя.
     * Позволяет задать параметры фильтрации
     * @function
     * @name getInfo(2)
     * @variation 2
     * @memberof mailru.friends
     * @param {{}} params
     * @param {number} [params.offset=0] - задает смещение в выборке, нужно если у пользователя контактов больше, чем лимит
     * @param {number} [params.limit=500] - задает лимит кол-ва получаемых контактов
     * @param {boolean} [params.extended=false] - не обязательный параметр. если передан true - возвращает полный набор полей о пользователях, но будет происходить долгий сетевой запрос, если false - будет возвращен урезанный набор полей - nick, online, uin, avatars
     * @param {mailru.friends~pick_callback} params.callback - принемает информацию о пользователях.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     * @example
     * mailru.friends.getInfo({
     *   offset: 1,
     *   limit: 1,
     *   callback: function(data){
     *     if (data.status == 'success') {
     *       alert('Your second friend is '+data.users[0].nick);
     *     } else {
     *       alert('Sorry, we have any error');
     *     }
     *   }
     * });
     */
    api.friends.getInfo = prepareFunc({
        'number': ['limit','offset'],
        'required,function': 'callback'
    },function(params){
        api.invoke('friends.getInfo'+(params.extended? 'Extended' : ''), {
            params: params,
            cb: function(data){
                try{
                    data = JSON.parse(data.replace(/\n/g,'\\n'));
                } catch(e){}
                params.callback(data);
            }
        });
    },{limit: 100, offset: 0},undefined,'friends.getInfo');

    /**
     * @callback mailru.photos~get_callback
     * @param {{}} result
     * @param {string_base64[]} result.photos - массив выбранных изображений в формате base64
     * @param {string} result.status - статус выполнения [success/error/fail]
     */
    /**
     * Показывает диалог выбора фото из галереи телефона/снять новое фото. после выбора фото и закрытия диалога приложение получает фото в формате base64. если возможен - нативный мультивыбор
     * @function get
     * @memberof mailru.photos
     * @param {mailru.photos~get_callback} callback - функция для обработки результата.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.photos.get = prepareFunc({
        'required,function': 'callback'
    },'photos.get');

    /**
     * Сохраняет изображение на устройство, в галлерею пользователя
     * Обязательным параметром является один из params.base64, params.url.
     * @function save
     * @memberof mailru.photos
     * @param {string_base64} params.base64 - изображение в формате base64.
     * @param {url} params.url - ссылка на изображение.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.photos.save = prepareFunc({
        'required': 'url|base64',
        'string': 'base64',
        'url': 'url'
    },'photos.save');

    /**
     * @callback mailru.photos~up_callback
     * @param {{}} result
     * @param {string} result.fileid - id загруженного ресурса.
     * @param {string} result.filesize - размер загруженного ресурса в байтах.
     * @param {string} result.filename - имя загруженного ресурса.
     * @param {string} result.mime - mime-тип содержимого загруженного ресурса.
     * @param {string} result.static_url - постоянная ссылка на загруженный ресурс.
     * @param {string} result.status - статус выполнения [success/error/fail]
     */
    /**
     * Загружает изображение на files.icq.com
     * Обязательным параметром является один из params.base64, params.url.
     * @function upload
     * @memberof mailru.photos
     * @param {string_base64} params.base64 - изображение в формате base64.
     * @param {url} params.url - ссылка на изображение.
     * @param {mailru.photos~up_callback} params.callback - ссылка на изображение.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.photos.upload = prepareFunc({
        'required': 'url|base64',
        'string': 'base64',
        'url': 'url',
        'function': 'callback'
    },'photos.upload');

    /**
     * @callback mailru.camera~get_callback
     * @param {{}} result
     * @param {string_base64[]} result.photos - массив выбранных изображений в формате base64
     * @param {string} result.status - статус выполнения [success/error/fail]
     */
    /**
     * Получить изображение с камеры
     * @function get
     * @memberof mailru.camera
     * @param {mailru.camera~get_callback} params.callback - функция для обработки результата.
     * @return {undefined|boolean} Если false значит метод выполнен с ошибками.
     */
    api.camera.get = prepareFunc({
        'required,function': 'callback'
    },'camera.get');

    window.mailru = api;
})(window.mailru, window.$);
